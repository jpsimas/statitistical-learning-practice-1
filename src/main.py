# PCS5024 - Practice 1
# Copyright (C) 2021  João Pedro de O. Simas
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import numpy as np
import yaml
import time

from sklearn import neighbors
from sklearn import tree
from sklearn import neural_network
from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.model_selection import cross_val_score, GridSearchCV, RandomizedSearchCV

import itertools

## map categorical values into numerical ones following the order they
## were given in the dictionaries
def processData(dataRaw, dicts):
    data = np.zeros((dataRaw.shape[0], dataRaw.shape[1]), dtype=float);

    # Build numerical dataset matrix (and transpose data)
    for i in range(0, dataRaw.shape[1]):
        for j in range(0, dataRaw.shape[0]):
            if(len(dicts[i]) != 1 or dataRaw[j, i] == "?"):
                data[j, i] = dicts[i][dataRaw[j, i]]
            else:
                data[j, i] = float(dataRaw[j, i])

    return data

## read data and metadata from files, map categorical values into
## numerical ones according to the order given in the metadata and
## finally normalize it so each attribute has zero main and unit variance.
def readData(path, prefix):
    ## Load Metadata
    file = open(path + prefix + ".yaml")
    metadata = yaml.load(file, Loader=yaml.FullLoader)
    file.close()
    
    ## Build attribute label name list
    attr_labels = []
    for key in metadata.keys():
        attr_labels.append(key)

    ## Load Data
    extension = ".data"
    dataRaw = np.genfromtxt(path + prefix + extension, delimiter=',', dtype="|U", autostrip=True)
                
    ## Build dictionaries for string labels to numerical values
    ## translation for each attribute
    dicts = []
    i = 0
    for key in metadata.keys():
        dicts.append({"?" : -1})
        j = 0
        if(len(metadata[key]) != 1): #if not continuous
            for label in metadata[key]:
                dicts[i][label] = j
                j += 1
        i += 1    

    # store label mappings to yaml file
    file = open("../out/dicts.yaml", "w")
    yaml.dump(dicts, file)
    file.close()

    # map categorical values using dictionary
    data = processData(dataRaw, dicts)

    #  Scaling factor vectors
    s = np.zeros((data.shape[1], 1), dtype=float);# std dev
    m = np.zeros((data.shape[1], 1), dtype=float);# mean
    
    ## Normalize/unbias columns (Except class colum)
    for i in range(0, data.shape[1] - 1):
        m[i] = np.mean(data[:, i])
        s[i] = np.std(data[:, i]) + 1e-12
        data[:, i] = (data[:, i] - m[i])/s[i]

    extension = ".test"

    dataTestRaw = np.genfromtxt(path + prefix + extension, delimiter=',', dtype="|U", autostrip=True, skip_header=1)
    dataTest = processData(dataTestRaw, dicts)
    
    # scale data
    for i in range(0, dataTest.shape[1] - 1):
        dataTest[:, i] = (dataTest[:, i] - m[i])/s[i]

    return (data, dataTest, attr_labels, dicts)

## calculate the mean accuracies from the obtained values from nFold
## cross-validation of some classifier model
def cross_val_mean_acc(model, nFold):
    accCV = cross_val_score(model, data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int), cv = nFold)
    acc = np.mean(accCV)
    return acc

##Attribute labeling
def labelAttr(x, y, attrList):
    return 0

## Main Function

## Print Copyright Notice
print("practice1  Copyright (C) 2021  João Pedro de Omena Simas\n"\
      "This program comes with ABSOLUTELY NO WARRANTY;\n"\
      "This is free software, and you are welcome to redistribute it\n"\
      "under certain conditions;\n")
    
path = "../data/"
prefix = "adult"

(data, dataTest, attr_labels, dicts) = readData(path, prefix)

# Test if columns are redundant
H, xedges, yedges = np.histogram2d(data[:, 3], data[:, 4], bins = [len(dicts[3]), len(dicts[3])], density = True)
H = H.T

plt.matshow(H, cmap="hot")
plt.colorbar()

## Plot Correlation Heat Map
corr = np.corrcoef(data.T)

plt.matshow(np.abs(corr), cmap = "hot")
plt.colorbar()
plt.xticks(ticks = range(0, len(attr_labels)), labels = attr_labels, rotation = 90)
plt.yticks(ticks = range(0, len(attr_labels)), labels = attr_labels)

## Attribute Selection
# remove attributes with correlation < 1e-2 with the output
excluded_cols = [] #force exclude redundant attribute education
for i in range(corr.shape[0] - 1, -1, -1):
    if(np.abs(corr[corr.shape[0] - 1, i]) < 1e-2):
        excluded_cols.append(i)

for i in excluded_cols:
    print("Excluded attribute " + str(i) + ": " + attr_labels[i] + ". corr: {:e}".format(corr[corr.shape[0] - 1, i]))
    
    data = data[:, np.r_[0:i, (i + 1):data.shape[1]]]
    dataTest = dataTest[:, np.r_[0:i, (i + 1):dataTest.shape[1]]]
    
    attr_labels.pop(i)

#compute covariance
corr = np.corrcoef(data.T)
    
# Plot Correlation Heat Map with attributes removed
plt.matshow(np.abs(corr), cmap = "hot")
plt.colorbar()
plt.xticks(ticks = range(0, len(attr_labels)), labels = attr_labels, rotation = 90)
plt.yticks(ticks = range(0, len(attr_labels)), labels = attr_labels)

## Train and validate models

# KNN
print("KNN:")
## Hyperparparameter Optimization
# try to open data file
nFold = 3
maxN = 40

accuracies = np.zeros((maxN, 1), dtype=float)

foundResults = True
try:
    file = open("../out/knnParams.yaml")
    params = yaml.load(file, Loader=yaml.FullLoader)
    nMax = int(params["k"])
    file.close()
    accuracies = np.genfromtxt("../out/accuracies.csv", delimiter=',', dtype="float", autostrip=True)
except IOError:
    foundResults = False
    
if(not foundResults):
    for i in range(0, maxN):
        t = time.time()
        accuracies[i] = cross_val_mean_acc(neighbors.KNeighborsClassifier(n_neighbors = (i + 1)), nFold)
        t = time.time() - t
    
        print("N = " + str(i + 1) + ": acc = " + str(accuracies[i, 0]) + ". time: " + str(t) + " s.")

    # Find optimal point according to criteria
    indMax = np.argmax(accuracies)
    maxAcc = accuracies[indMax]
    tol = 1e-3
    for i in range(0, maxN):
        if(np.abs(accuracies[i] - maxAcc) <= tol):
            indMax = i
            maxAcc = accuracies[i]
            break

    nMax = indMax + 1

    print("found optimal point: N = " + str(nMax) + ". acc = " + str(maxAcc))
    
    # export 
    file = open("../out/knnParams.yaml", "w")
    yaml.dump({"k" : nMax, "accCv" : float(maxAcc[0])}, file)
    file.close()
    np.savetxt("../out/accuracies.csv", accuracies, delimiter=",")
else:
    print("KNN parameters found. Skipping optimization. To re-run delete ../out/knnParams.yaml")
        
plt.figure(4)
plt.plot(accuracies)
plt.title("k")
plt.ylabel("Accuracy")
plt.xlabel("k")

# Validate with test data set

modelKNN = neighbors.KNeighborsClassifier(n_neighbors = nMax)
modelKNN.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))

#Figures of Merit
classPred = modelKNN.predict(dataTest[:, 0:(dataTest.shape[1] - 1)])
accuracy = accuracy_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
precision = precision_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
recall = recall_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)

print("Accuracy: {}".format(accuracy))
print("Precision: {}".format(precision))
print("Recall: {}".format(recall))

file = open("../out/knnResults.yaml", "w")
yaml.dump({"accuracy" : float(accuracy), "precision" : float(precision), "recall" : float(recall)}, file)
file.close()

## Decision tree
print("Decision Tree:")

# check if there are stored results
foundResults = True
try:
    file = open("../out/treeParams.yaml")
    params = yaml.load(file, Loader=yaml.FullLoader)
    max_depth = int(params["max_depth"])
    max_features = int(params["max_features"])
    min_samples_leaf = int(params["min_samples_leaf"])
    criterion = params["criterion"]
    file.close()
except IOError:
    foundResults = False

## Hyperparameter Optimization (random cross val)
# Parameters being tuned: max_depth, min_sample_leaf, max_features

if(not foundResults):
    paramVals = {"max_depth": list(range(1, 15)) + [None],
                 "max_features": range(3, 12),
                 "min_samples_leaf": range(1, 10),
                 "criterion": ["gini", "entropy"]}

    treeCrossValSearch = GridSearchCV(tree.DecisionTreeClassifier(random_state = 0), paramVals, cv = nFold, verbose = 1)

    treeCrossValSearch.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))

    print("optimal params: {}".format(treeCrossValSearch.best_params_))
    print("cross val acc {}".format(treeCrossValSearch.best_score_))
    
    max_depth = treeCrossValSearch.best_params_["max_depth"]
    max_features = treeCrossValSearch.best_params_["max_features"]
    min_samples_leaf = treeCrossValSearch.best_params_["min_samples_leaf"]
    criterion = treeCrossValSearch.best_params_["criterion"]
    
    # store parameters to yaml file
    file = open("../out/treeParams.yaml", "w")
    params = treeCrossValSearch.best_params_
    params["accCv"] = float(treeCrossValSearch.best_score_)
    yaml.dump(params, file)
    file.close()
else:
    print("Decision Tree parameters found. Skipping optimization. To re-run delete ../out/treeParams.yaml")
    
modelTree = tree.DecisionTreeClassifier(random_state = 0, max_depth = max_depth, max_features = max_features, min_samples_leaf = min_samples_leaf, criterion = criterion)
modelTree.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))

classPred = modelTree.predict(dataTest[:, 0:(dataTest.shape[1] - 1)])
accuracy = accuracy_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
precision = precision_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
recall = recall_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)

print("Accuracy: {}".format(accuracy))
print("Precision: {}".format(precision))
print("Recall: {}".format(recall))

file = open("../out/treeResults.yaml", "w")
yaml.dump({"accuracy" : float(accuracy), "precision" : float(precision), "recall" : float(recall)}, file)
file.close()

# MLP
print("MLP:")

# check if there are stored results
foundResults = True
try:
    file = open("../out/mlpParams.yaml")
    params = yaml.load(file, Loader=yaml.FullLoader)
    hidden_layer_sizes = params["hidden_layer_sizes"]
    activation = params["activation"]
    file.close()
except IOError:
    foundResults = False

if(not foundResults):
    mlpDims = [(7,), (8,), (9,), (7, 7), (8, 8), (9, 9), (7, 7, 7), (8, 8, 8), (9, 9, 9), (8, 10, 10)]
    
    paramVals = {"activation": ["tanh"],
                 "hidden_layer_sizes": mlpDims}

    MLPCrossValSearch = GridSearchCV(neural_network.MLPClassifier(max_iter = 1000, random_state = 0), paramVals, cv = nFold, verbose = 4)
    MLPCrossValSearch.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))

    print("optimal params: {}".format(MLPCrossValSearch.best_params_))
    print("cross val acc {}".format(MLPCrossValSearch.best_score_))

    # store parameters to yaml file
    file = open("../out/mlpParams.yaml", "w")
    params = MLPCrossValSearch.best_params_
    params["accCv"] = float(MLPCrossValSearch.best_score_)
    yaml.dump(params, file)
    file.close()

    activation = MLPCrossValSearch.best_params_["activation"]
    hidden_layer_sizes = MLPCrossValSearch.best_params_["hidden_layer_sizes"]
else:
    print("MLP parameters found. Skipping optimization. To re-run delete ../out/mlpParams.yaml")

t = time.time()
modelMLP = neural_network.MLPClassifier(max_iter = 1000, random_state = 0, hidden_layer_sizes = hidden_layer_sizes, activation = activation)
modelMLP.fit(data[:, 0:(data.shape[1] - 1)], data[:, data.shape[1] - 1].astype(int))
t = time.time() - t
print("MLP Run time: " + str(t) + " s.")

classPred = modelMLP.predict(dataTest[:, 0:(dataTest.shape[1] - 1)])
accuracy = accuracy_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
precision = precision_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)
recall = recall_score(dataTest[:, dataTest.shape[1] - 1].astype(int), classPred)

print("Accuracy: {}".format(accuracy))
print("Precision: {}".format(precision))
print("Recall: {}".format(recall))

file = open("../out/mlpResults.yaml", "w")
yaml.dump({"accuracy" : float(accuracy), "precision" : float(precision), "recall" : float(recall)}, file)
file.close()


plt.figure(1)#correlation
plt.savefig("../img/educHist.eps")

plt.figure(2)#correlation
plt.savefig("../img/correlation.eps")

plt.figure(3)#correlation excluded params
plt.savefig("../img/correlationExcl.eps")

plt.figure(4)#knn accuracies
plt.savefig("../img/knnAcc.eps")

plt.show()
