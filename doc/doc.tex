% Created 2021-05-15 Sat 02:26
% Intended LaTeX compiler: pdflatex
\documentclass[10pt, a4paper, twocolumn]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{placeins}
\author{João Pedro de Omena Simas (9349429)}
\date{}
\title{Practice: Classifiers for the Adult Dataset}
\hypersetup{
 pdfauthor={João Pedro de Omena Simas (9349429)},
 pdftitle={Practice: Classifiers for the Adult Dataset},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.4.4)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section{Introduction}
\label{sec:orgd0284ea}
The purpose of this report is to evaluate the performance of a few
classifiers with the Adult dataset from
\cite{asuncion2007uci}. All code used to generate the presented
results can be found in
\url{https://gitlab.com/jpsimas/statitistical-learning-practice-1}. 
\section{Preprocessing}
\label{sec:org0058330}
\subsection{Label Encoding}
\label{sec:orgc928dc4}
To encode the categorical features, the values were indexed
sequentially, in the order they were given in the dataset
description file (adult.names).
\subsubsection{Missing Data}
\label{sec:org1dac877}
To deal with missing data, the missing attributes were filled with
the numerical value -1, as all attributes are positive, to take
advantage of the fact that there's correlation between the fields
"country-of-origin", "workclass" and "occupation" being missing and the
class (">50k"/"<=50k"). This can be seen by the fact that as when each
of these attributes are missing the estimated probabilities of the
class being ">50k" are approximately and respectively 0.104, 0.104
and 0.25.
\subsection{Scaling/Normalization}
\label{sec:orgd694840}
To scale the data prior to training the classifiers, the standard deviation and
mean of each attribute was calculated using the training dataset.
After that, each attribute, from both training and test datasets,
was decremented by the mean and subsequently divided by the
standard deviation, in order to obtain attributes that have zero
mean and unit variance, that is, assuming the test data has similar
statistical properties as the training data.
\subsection{Attribute Selection}
\label{sec:orgce3e784}
In order to try to reduce the complexity of the models obtained,
some preliminary analysis is done on the input data, in order to
evaluate if all attributes contains any information related to the
final decision. The criterion chosen is to exclude any
attribute whose correlation coefficient with the output class
(numerically encoded in the same way a described for the
attributes) is less than 10\textsuperscript{-2}. That excludes "occupation",
"education" and "fnlwgt" from the data used by the
classifier, keeping a total of 11 attributes. Below, a
visualization of the attribute (including the class (">50k"/"<=50k")
and the excluded ones) correlations can be seen:
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/correlation.eps}
\caption{Absolute value of the correlation coefficients of the attributes estimated from the training dataset.}
\end{figure}

\section{Hyperparameter optimization}
\label{sec:orgd5a3cee}
\subsection{K-Nearest Neighbor}
\label{sec:org55aec66}
In order optimize the order of the k-NN algorithm used for
classification, the accuracy is estimated using 3-fold cross-validation
for values of k between 1 and 40. Then, the value that 
maximizes it is taken and used to find the lowest k whose accuracy
differs by less than 10\textsuperscript{-3}.
For this data and criteria the optimum was found to be k =
20, 
with mean 3-fold cross-validation accuracy of
0.8439. 
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{../img/knnAcc.eps}
\caption{k-NN mean 3-fold cross-validation accuracy in function of the order of the classifier.}
\end{figure}
\subsection{Classification Tree}
\label{sec:org4e1058a}
For the classification tree, the approach was similar, but with
multiple parameters and without the final simplification step. The
hyperparameters chosen to be optimized were the metric (gini
impurity or information gain), maximum depth, maximum features to
consider for finding the best split and minimum number of samples
at a leaf node.

Doing the same process of 3-fold cross-validation over all combination
of values in the ranges 1-14, 3-11 and 1-9, respectively, for the
numerical parameters, and alternating between gini impurity and
information gain as metrics for the tree-generation algorithm,
the optimal hyperparameters were determined to be
max\_depth = 10, 
max\_features = 11 and
min\_samples\_leaf = 3
with the
information gain
used as metric. The mean 3-fold cross-validation accuracy obtained in
this case was 0.8566. 
\subsection{Multi-Layer Perceptron}
\label{sec:org29cfab9}
In the case of the MLP, the same approach as the classification
tree was used, but a smaller search space had to be employed due to
the large amount of time it takes to train this type of model. The
optimized parameters were the activation function (either tanh or
ReLU) and the hidden layer sizes.
After searching over a number of combinations, the obtained hyperparameters\footnote{ReLU and tanh activation functions and inner layer dimensions of (7),
(8), (9), (7, 7), (8, 8), (9, 9), (7, 7, 7), (8, 8, 8), (9, 9, 9) and
(8, 10, 10).

\bibliography{references}
\bibliographystyle{ieeetr}}
were tanh
activation with an MLP with hidden layers of dimensions
(8, 10, 10).
In this case, the achieved mean 3-fold cross-validation accuracy
was of 0.8527. 
\section{Results}
\label{sec:org0c1e897}
Below, the accuracies of each classifier type with the
hyperparameters found in the previous section, obtained by training
them with the entirety of the training dataset and testing them
with the test dataset are presented:

\FloatBarrier
\begin{table}[htbp]
\centering
\begin{tabular}{|l|l|l|l|}
\hline
 & K-Nearest & Classifica- & Multi-Layer\\
 & Neighbor & tion Tree & Perceptron\\
\hline
Accuracy & 0.844 & 0.8577 & 0.8553\\
\hline
\end{tabular}
\caption{Accuracies of each classification model with the test dataset.}

\end{table}

\FloatBarrier
Therefore, the best of the three estimators, in terms of accuracy,
is the classification tree. Additionally, it's accuracy, precision and recall
are:

\FloatBarrier
\begin{table}[htbp]
\centering
\begin{tabular}{|r|r|r|}
\hline
Accuracy & Precision & Recall\\
\hline
0.8577 & 0.8688 & 0.9585\\
\hline
\end{tabular}
\caption{Figures of merit of the obtained classification tree in the test dataset.}

\end{table}
\FloatBarrier
\end{document}
